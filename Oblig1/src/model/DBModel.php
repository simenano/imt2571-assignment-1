<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db=null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            try{
				$this->db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4','root','', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			}catch(PDOException $ex) {
				echo "Error: ".$ex->getMessage();
			}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try{
			//$bookList = array(); #PHP
			$stmt = $this->db->query('SELECT * FROM book ORDER BY id');
			while($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$bookList[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
			}	
			return $bookList;
		}catch(PDOException $ex){
			echo $ex->getMessage();
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = NULL;
		try{
			$stmt = $this->db->prepare('SELECT * FROM book WHERE id=:id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			if($id > 0){
				$stmt->execute();
				$rows = $stmt->fetch(PDO::FETCH_ASSOC);
				if($rows){
					$book = new Book($rows['title'], $rows['author'], $rows['description'], $rows['id']);
					return $book;
				}else{
					return NULL;
				}
			}//else{return true;}
		}catch(PDOException $ex){
			echo "No book ".$ex->getMessage();
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
			$stmt = $this->db->prepare('INSERT INTO book (title, author, description) VALUES(:title, :author, :description)');
			//$stmt->bindValue(':id', $book->id, PDO::PARAM_INT);
			$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
			$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
			$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
			if(empty($book->title) || empty($book->author)){
				//$stmt->execute(); 
			}
			else{
					$stmt->execute();
					$book->id = $this->db->lastInsertId();
			}
			
		}catch(PDOException $ex){
			echo "Invalid entry ".$ex->getMessage();
		}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try{
			$stmt = $this->db->prepare('UPDATE book SET title = :title, author = :author, description = :description WHERE id = :id');
			$stmt->bindValue(':id', $book->id, PDO::PARAM_INT);
			$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
			$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
			$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
			if(empty($book->title) || empty($book->author)){
				return NULL; 
			}
			else{
					$stmt->execute();
					$book->id = $this->db->lastInsertId();
			}
			//$stmt->execute();
		}catch(PDOException $ex){
			echo "Invalid entry ".$ex->getMessage();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
			$stmt = $this->db->prepare('DELETE FROM book WHERE id = :id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			$stmt->execute();
			//$this->id = $this->db->lastInsertId();
			//return $this->id;
		}catch(PDOException $ex){
			echo $ex->getMessage();
		}
    }
	
}

?>